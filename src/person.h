#ifndef PERSON_H
#define PERSON_H

#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/character_body3d.hpp>
#include <godot_cpp/classes/area3d.hpp>
#include <godot_cpp/classes/animation_player.hpp>
#include <godot_cpp/classes/timer.hpp>

namespace godot {

class Person : public CharacterBody3D {
	GDCLASS(Person, CharacterBody3D)

private:
	Vector3 goal;
	float speed;
	bool move;
	bool attributes[4];
	bool searched;

	AnimationPlayer* myAP;		// for person animations
	Timer* myTimer;				// for clumsy

	Vector3 goal_direction;		// storing goal direction (non-zero when paused)
	Vector3 current_velocity;	// storing velocity (non-zero when paused)
	bool paused;
	Vector3 last_position; 
	Person* pGoal;
	Person* partner;

protected:
	static void _bind_methods();

public:
	Person();
	~Person();

	void _physics_process(double delta) override;
	void _ready();
	void set_goal(Vector3 goal);
	void _toggle_movement();
	void incomingPerson(Node3D* incomingNode);
	void set_speed(float p_speed);

	// getters and setters for search/attached
	void set_attributes(TypedArray<bool> p_attributes);
	TypedArray<bool> get_attributes();
	bool get_searched();
	void set_searched(bool s);
	void set_pGoal(Person* myGoal);
	void set_partner(Person* pPartner);
	int crowdType;
	void print_vec(String message, Vector3 vec);
	void finish_anim(String animation_name);
	void timer();
};

}

#endif