#ifndef CROWD_HANDLER_H
#define CROWD_HANDLER_H

#include "person.h"
#include <godot_cpp/classes/node3d.hpp>


namespace godot {

class CrowdHandler : public Node3D {
  GDCLASS(CrowdHandler, Node3D)

private:
    int personCount;
    Array personLocations;
    Vector3 startPoint;
    Vector3 endPoint;
    int start_width;
    int start_height;
    bool move_toggle;
    

protected:
  static void _bind_methods();

public:
  CrowdHandler();
  ~CrowdHandler();
  Signal toggle_movement;

  void _physics_process(double delta);
  void _ready();

  void spawnPeople();
  void resetCrowd();
  Vector3 generate_person_positions();

  // getters and setters
  void set_person_count(int p_person_count);
  int get_person_count();
  void set_width(int p_width);
  int get_width();
  void set_height(int p_height);
  int get_height();
  void set_move_toggle(bool p_move_toggle);
  int get_move_toggle();
  
};

} // namespace godot

#endif