extends Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("swap_camera"):  # Check if the "ui_accept" action is pressed
		if $Camera3D.current:
			$Camera3D.clear_current()
		else:
			$Camera3D2.clear_current()
