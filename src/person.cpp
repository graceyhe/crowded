#include "person.h"

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/area3d.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/classes/collision_shape3d.hpp>
#include <godot_cpp/classes/animation_player.hpp>
#include <godot_cpp/classes/shape3d.hpp>
#include <godot_cpp/classes/sphere_shape3d.hpp>
#include <godot_cpp/classes/cylinder_shape3d.hpp>
#include <godot_cpp/classes/random_number_generator.hpp>
using namespace godot;

void Person::_bind_methods() {
    ClassDB::bind_method(D_METHOD("_toggle_movement"), &Person::_toggle_movement);
    ClassDB::bind_method(D_METHOD("set_speed"), &Person::set_speed);
    ClassDB::bind_method(D_METHOD("finish_anim"), &Person::finish_anim);
    ClassDB::bind_method(D_METHOD("timer"), &Person::timer);

}

Person::Person() {
    paused = true;
    speed = 3.0;
    move = true;
    current_velocity = Vector3(1, 0, 0) * speed;
    searched = false;
    pGoal = NULL;
    partner = NULL;
    crowdType = 0;
}

Person::~Person() {
	
}

void Person::_ready() {
    get_parent()->connect("toggle_movement", Callable(this, "_toggle_movement"));
    myAP = Object::cast_to<AnimationPlayer>(get_node_or_null("player/AnimationPlayer"));
    myAP->connect("animation_finished", Callable(this, "finish_anim"));
    myTimer = Object::cast_to<Timer>(get_node_or_null("Timer"));
    myTimer->connect("timeout", Callable(this, "timer"));
    
    if(attributes[3]) {
        set_position(get_global_position() + Vector3(15, 0, 0));
    } else {
        Vector3 end_point = Vector3(30, 0, 0);
        if(crowdType == 2) {    // open field - randomize direction
            RandomNumberGenerator rng;
            rng.randomize();
            float random_rads = rng.randf_range(0.0, 6.283);
            end_point.rotate(Vector3(0, 1, 0),  random_rads);
        } 
        goal = get_global_position() + end_point;
    }
}

void Person::_physics_process(double delta) {
    if (Engine::get_singleton()->is_editor_hint()) return;
    Vector3 currPosition = get_global_position();
    // determine goal
    if(attributes[3] && pGoal) {
        goal_direction = pGoal->get_global_position() - get_global_position();
        goal = pGoal->get_global_position();
    } else if(crowdType == 1) {     // tour group moves straight by default
        goal_direction = Vector3(1, 0, 0);
    } else {
        goal_direction = (goal - get_global_position()).normalized();
    }
    Vector3 goal_vector = goal_direction;
    goal_vector.normalize();
    // get influencing bodies (gravity fields)
    Area3D *area = Object::cast_to<Area3D>(get_node_or_null("personalSpace"));
    TypedArray<Area3D> overlappingBodies = area->get_overlapping_areas();
    Vector3 push_acceleration = Vector3(0, 0, 0);
    for(int i = 0; i < overlappingBodies.size(); i++) {
        Variant element = overlappingBodies[i];
        Area3D *overlappingBody = Object::cast_to<Area3D>(element);
        // found "obstacle"
        if(overlappingBody) {
            if(String(overlappingBody->get_parent()->get_name()) != "Ground" && !(pGoal && pGoal->get_name() == overlappingBody->get_parent()->get_name())
                    && !(partner && partner->get_name() == overlappingBody->get_parent()->get_name())) {   
                Vector3 otherPosition = Object::cast_to<Node3D>(overlappingBody->get_parent())->get_global_position();
                Vector3 push = -(otherPosition - currPosition);
                float eta = 200.0;
                float q_star = 2.0;
                // instantiate q_star (max relevant radius around obstacle) based on collision radius
                CollisionShape3D* col = overlappingBody->get_node<CollisionShape3D>("CollisionShape3D");
                Shape3D* shape = *(col->get_shape());
                if(shape->is_class("SphereShape3D")) {
                    q_star = Object::cast_to<SphereShape3D>(shape)->get_radius();
                } else if(shape->is_class("CylinderShape3D")) {
                    q_star = Object::cast_to<CylinderShape3D>(shape)->get_radius();
                }
                // determine if the person is moving towards obstacle
                float proximity = push.length();
                float delta_q = 1.0;
                if(current_velocity.normalized().dot((otherPosition - currPosition).normalized()) > 0.0) {  // moving towards obstacle
                    delta_q = -1.0;
                }
                // adjust push direction for standstill conditions
                if(acos(push.dot(get_velocity()) / (get_velocity().length() * push.length())) < 0.001) {
                    push.rotate(Vector3(0, 1, 0), delta_q * 0.01);
                }
                // determine true proximity (distance from person to object is person's outer radius to object)
                float true_proximity = proximity;
                float gradient_repulsive = eta * (1 / q_star - 1 / true_proximity) * (1 / q_star - 1 / true_proximity);
                float force = gradient_repulsive;
                push_acceleration += push.normalized() * force;
            } 
        }
    }
    /* UNCOMMENT THIS FOR ACCELERATION CAP
    if(push_acceleration.length() > 750.0) {
        push_acceleration = push_acceleration.normalized() * 750.0;
    }
    */

    // incorporate avoidance/separation
    Vector3 push_velocity = push_acceleration * delta;
    float speed_left = 0;
    if(push_velocity.length() < speed) {
        speed_left = speed - push_velocity.length();
    }
    goal_vector = goal_direction.normalized() * speed_left + push_acceleration * delta;

    // incorporate cohesion and alignment if crowd type is tour group
    if(crowdType == 1) {
        TypedArray children = get_parent()->get_children();
        Vector3 average_position = Vector3(0, 0, 0);
        Vector3 average_velocity = Vector3(0, 0, 0);
        for(int i = 0; i < children.size(); i++) {
            CharacterBody3D* child = Object::cast_to<CharacterBody3D>(children[i]);
            average_position += child->get_global_position();
            average_velocity += child->get_velocity();
        }
        average_position /= children.size();
        average_velocity /= children.size();
        Vector3 cohesion = average_position - get_global_position();
        Vector3 alignment = average_velocity - get_global_position();
        goal_vector += 0.25 * cohesion.normalized();
        goal_vector += 0.25 * alignment.normalized();
    }

    if(get_global_position().distance_to(goal) <= 2 && pGoal != NULL) {
        return;
    }
    // move person
    if(!paused && move) {
        set_velocity(goal_vector);
        myAP->play("walking");
        if(crowdType == 1) {
            look_at(get_global_position() + get_velocity());
        } else {
            look_at(goal);
        }
        
        current_velocity = goal_vector;
    } else{
        set_velocity(Vector3(0, 0, 0));
    }
    move_and_slide();
}

// for debugging purposes
void Person::print_vec(String message, Vector3 vec) {
    UtilityFunctions::print(message + ":" + String::num(vec.x) + ", " + String::num(vec.y) + ", " + String::num(vec.z));
}

void Person::set_goal(Vector3 goal) {
    this->goal = goal;
}

void Person::_toggle_movement() {
    paused = !paused;
}

void Person::set_speed(float p_speed) {
    speed = p_speed;
}

void Person::finish_anim(String animation_name) {
    if(animation_name == "tripping") {
        move = true;
    }
}

void Person::set_attributes(TypedArray<bool> p_attributes) {
    for(int i = 0; i < p_attributes.size(); i++) {
        attributes[i] = p_attributes[i];
    }
}

TypedArray<bool> Person::get_attributes() {
    TypedArray<bool> attrs[4];
    for(int i = 0; i < 4; i++) {
        attrs->append(attributes[i]);
    }
    return *attrs;
}

bool Person::get_searched() {
    return searched;
}

void Person::set_searched(bool s) {
    searched = s;
}

void Person::set_pGoal(Person* myGoal) {
    pGoal = myGoal;
    goal = pGoal->get_global_position();
}

void Person::set_partner(Person* pPartner) {
    partner = pPartner;
}

void Person::timer() {
    RandomNumberGenerator rng;
    rng.randomize();
    float random_speed = rng.randf_range(0.0, 100.0);
    if(attributes[0] && random_speed < 50.0 && !paused) {
        myAP->play("tripping");
        move = false;
        set_velocity(Vector3(0, 0, 0));
    }
    if(get_global_position().distance_to(last_position) < 1) {
        set_global_position(get_global_position() + 2 * get_velocity().normalized());
    }
    last_position = get_global_position();
}