extends Node2D
var traits = [false, false, false, false, false]
var crowdType = 0
var maxSpeed = 5
var minSpeed = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_traits_enable_toggled(toggled_on):
	if(toggled_on):
		traits[0] = true
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Clumsy.disabled =  false
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Nervous.disabled =  false
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Attached.disabled =  false
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Searching.disabled =  false

	else:
		traits[0] = false
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Clumsy.disabled =  true
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Nervous.disabled =  true
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Attached.disabled =  true
		$CanvasLayer/MarginContainer/VBoxContainer/VBoxContainer/Searching.disabled =  true

func _on_h_slider_value_changed(value):
	$CanvasLayer/MarginContainer/VBoxContainer/PeopleSlider/Label.text = str(value)
	$"../CrowdHandler".set_person_count(value)
	
func _on_clumsy_toggled(toggled_on):
	traits[1] = toggled_on
	$"../CrowdHandler".resetCrowd()
	
func _on_nervous_toggled(toggled_on):
	traits[2] = toggled_on
	$"../CrowdHandler".resetCrowd()

func _on_attached_toggled(toggled_on):
	traits[3] = toggled_on
	$"../CrowdHandler".resetCrowd()

func _on_searching_toggled(toggled_on):
	traits[4] = toggled_on
	$"../CrowdHandler".resetCrowd()

func _on_option_button_item_selected(index):
	crowdType = index
	$"../CrowdHandler".resetCrowd()

func _on_speed_value_changed(value):
	maxSpeed = value
	$CanvasLayer/MarginContainer/VBoxContainer/SpeedSlider/Label.text = str(value)

func _on_min_slider_value_changed(value):
	minSpeed = value
	$CanvasLayer/MarginContainer/VBoxContainer/MinSpeedSlider/Label.text = str(value)
