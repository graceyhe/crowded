#include "crowdhandler.h"
#include "person.h"
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/camera3d.hpp>
#include <godot_cpp/classes/sprite3d.hpp>
#include <godot_cpp/classes/scene_tree.hpp>
#include <godot_cpp/classes/window.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/resource_loader.hpp>
#include <godot_cpp/classes/packed_scene.hpp>
#include <godot_cpp/classes/random_number_generator.hpp>
#include <godot_cpp/classes/input.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/classes/node2d.hpp>
#include <godot_cpp/classes/collision_shape3d.hpp>
#include <godot_cpp/classes/sphere_shape3d.hpp>
#include <godot_cpp/classes/label3d.hpp>
using namespace godot;

void CrowdHandler::_bind_methods() {    
    ClassDB::bind_method(D_METHOD("get_person_count"), &CrowdHandler::get_person_count);
    ClassDB::bind_method(D_METHOD("set_person_count", "p_person_count"), &CrowdHandler::set_person_count);
    ADD_PROPERTY(PropertyInfo(Variant::INT, "person_count"), "set_person_count", "get_person_count");

    ClassDB::bind_method(D_METHOD("get_width"), &CrowdHandler::get_width);
    ClassDB::bind_method(D_METHOD("set_width", "p_width"), &CrowdHandler::set_width);
    ADD_PROPERTY(PropertyInfo(Variant::INT, "start_width"), "set_width", "get_width");

    ClassDB::bind_method(D_METHOD("get_height"), &CrowdHandler::get_height);
    ClassDB::bind_method(D_METHOD("set_height", "p_height"), &CrowdHandler::set_height);
    ADD_PROPERTY(PropertyInfo(Variant::INT, "start_height"), "set_height", "get_height");

    ClassDB::bind_method(D_METHOD("get_move_toggle"), &CrowdHandler::get_move_toggle);
    ClassDB::bind_method(D_METHOD("set_move_toggle", "p_move_toggle"), &CrowdHandler::set_move_toggle);
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "start_move_toggle"), "set_move_toggle", "get_move_toggle");

    ClassDB::bind_method(D_METHOD("resetCrowd"), &CrowdHandler::resetCrowd);
    ADD_SIGNAL(MethodInfo("toggle_movement"));
}

CrowdHandler::CrowdHandler() {
    personCount = 30;
    start_width = 5;
    start_height = 30;
    startPoint = Vector3(0, 0, 0);
    endPoint = Vector3(20, 0, 0);
}

CrowdHandler::~CrowdHandler() {
  // Add your cleanup here.
}

void CrowdHandler::_ready() {
    spawnPeople();
    
}

void CrowdHandler::_physics_process(double delta) {
    if (Engine::get_singleton()->is_editor_hint()) return;
    if(Input::get_singleton()->is_action_just_pressed("pause")) {
        emit_signal("toggle_movement");
    } 
    if(Input::get_singleton()->is_action_just_pressed("reset")) {
        resetCrowd();
    }
}

void CrowdHandler::spawnPeople() {
    godot::ResourceLoader* resource_loader = godot::ResourceLoader::get_singleton();
    Ref<godot::PackedScene> tLoaderScene = resource_loader->load("res://Scenes/person.tscn");
    // get enabled traits
    Node3D* world = Object::cast_to<Node3D>(get_parent());
    bool enabled_traits[5] = {false, false, false, false, false};
    int minSpeed = 3;
    int maxSpeed = 5;
    int crowdT = 0;
    if(world){
        Node2D* HUD = Object::cast_to<Node2D>(world->get_node_or_null("HUD"));
        if(HUD) {
            Variant traits = HUD->get("traits");
            if (traits.get_type() == Variant::ARRAY) {
                Array traits_array = traits;
                // Iterate over the array and convert each element to a boolean
                for (int i = 0; i < traits_array.size(); ++i) {
                    enabled_traits[i] = bool(traits_array[i]);
                }
            }
            Variant minS = HUD->get("minSpeed");
            Variant maxS = HUD->get("maxSpeed");
            Variant icrowdT = HUD->get("crowdType");
            minSpeed = minS;
            maxSpeed = maxS;
            crowdT = icrowdT;

        }
    }
    // spawn persons in the world
    for(int i = 0; i < personCount; i++) {
        Node* personLoaderNode = tLoaderScene->instantiate();
        Person* personLoader = Object::cast_to<Person>(personLoaderNode);
        Vector3 random_position = generate_person_positions();
        personLocations.append(random_position);
        personLoader->set_position(random_position);
        personLoader->set_goal(endPoint);
        RandomNumberGenerator rng;
        rng.randomize();
        float random_speed = rng.randf_range(minSpeed, maxSpeed);
        personLoader->set_speed(random_speed);
        TypedArray<bool> mytraits;
        if(enabled_traits[0]) { // set traits for individual. 16% chance for each trait
            rng.randomize();
            for(int i = 1; i < 5; i++) {
                float generate_rand = rng.randf_range(0.0, 1.0);
                if(generate_rand <= 0.16 && enabled_traits[i]) {    
                    mytraits.append(true);
                } else {
                    mytraits.append(false);
                }
            }
        } else {
            for(int i = 0; i < 5; i++) 
                mytraits.append(false);
        }
        personLoader->set_attributes(mytraits);
        personLoader->crowdType = crowdT;
        add_child(personLoader);

    }

    // set attributes for searching and attached
    for(int i = 0; i < personCount; i++) {
        Person* current = Object::cast_to<Person>(get_child(i));
        if(current) {
            TypedArray<bool> atts = current->get_attributes();
            if(enabled_traits[4] && atts[3]) {  // searching
                for(int j = 0; j < personCount; j++) {
                    Person* other = Object::cast_to<Person>(get_child(j));
                    if(j != i && other && !other->get_searched() && !other->get_attributes()[3]) {
                        current->set_pGoal(other);
                        other->set_searched(true);
                        other->set_partner(current);
                        break;
                    }
                }
            } 
            if(enabled_traits[2] && atts[1]) { // nervous
                Area3D *area = Object::cast_to<Area3D>(current->get_node_or_null("personalSpace"));
                CollisionShape3D* col = area->get_node<CollisionShape3D>("CollisionShape3D");
                Shape3D* shape = *(col->get_shape());
                if(shape->is_class("SphereShape3D")) {
                    float rad =  Object::cast_to<SphereShape3D>(shape)->get_radius();
                    Object::cast_to<SphereShape3D>(shape)->set_radius(rad * 1.5);
                }
                Label3D *label = Object::cast_to<Label3D>(current->get_node_or_null("Label3D"));
                label->show();
            }
        }
    }
}

void CrowdHandler::resetCrowd() {
    while(get_child_count() > 0) {
        Node* child = get_child(0);
        remove_child(child);
        child->queue_free();
    }
    spawnPeople();
}

Vector3 CrowdHandler::generate_person_positions() {
    Vector3 new_position = Vector3();
    float min_distance_from_person = 10.0;
    int attempts = 1000;
    while (true) {
        RandomNumberGenerator rng;
        rng.randomize();
        new_position = Vector3(startPoint.x + rng.randf_range(-start_width / 2, start_width / 2), startPoint.y, startPoint.z + rng.randf_range(-start_height / 2, start_height / 2));
        // Check if the new position does not overlap with existing positions
        bool valid = true;
        for (int j = 0; j < personLocations.size(); ++j) {
            Vector3 existing_position = personLocations[j];
            if (new_position.distance_to(existing_position) < min_distance_from_person) {
                valid = false;
                break;
            }
        }
        if (valid) {
            return new_position;
        }
        if(attempts <= 0) {
            min_distance_from_person-= 0.5;
            attempts = 1000;
        } else {
            attempts--;
        }
    }
}

int CrowdHandler::get_person_count() {
    return personCount;
}

void CrowdHandler::set_person_count(int p_person_count) {
    personCount = p_person_count;
    resetCrowd();
}

int CrowdHandler::get_width() {
    return start_width;
}

void CrowdHandler::set_width(int p_width) {
    start_width = p_width;
    resetCrowd();
}

int CrowdHandler::get_height() {
    return start_height;
}

void CrowdHandler::set_height(int p_height) {
    start_height = p_height;
    resetCrowd();
}

int CrowdHandler::get_move_toggle() {
    return move_toggle;
}

void CrowdHandler::set_move_toggle(bool p_move_toggle) {
    move_toggle = p_move_toggle;
    emit_signal("toggle_movement");
}

